<?php
declare(strict_types = 1);

namespace Rose\Framework\Router;

/**
 * Interface for router implementations
 *
 * @package Rose\Framework\Router
 * */
interface RouterInterface
{
	/**
	 * Method for GET
	 *
	 * @var string
	 */
	public const string METHOD_GET = 'GET';
	
	/**
	 * Method for GET
	 *
	 * @var string
	 */
	public const string METHOD_POST = 'POST';
	
	/**
	 * Method for GET
	 *
	 * @var string
	 */
	public const string METHOD_PUT = 'PUT';
	
	/**
	 * Method for GET
	 *
	 * @var string
	 */
	public const string METHOD_PATCH = 'PATCH';
	
	/**
	 * Method for GET
	 *
	 * @var string
	 */
	public const string METHOD_DELETE = 'DELETE';
	
	/**
	 * Method for GET
	 *
	 * @var string
	 */
	public const string METHOD_HEAD = 'HEAD';
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string|string[] $method
	 *    HTTP method for the route
	 *
	 * @param string $route
	 *    The route to match
	 *
	 * @param callable|array $handler
	 *    The callback to call when the route matches
	 */
	public function AddRoute(string|array $method, string $route, callable|array $handler) : void;
	
	/**
	 * Create a route group with a common prefix
	 *
	 * All routes created in the given callback will have the given group
	 * prefix prepended
	 *
	 * @param string $prefix
	 *    The route prefix
	 *
	 * @param callable|array $callback
	 *    The callback to call when the prefix matches
	 */
	public function AddGroup(string $prefix, callable|array $callback) : void;
	
	/**
	 * Process the routing
	 */
	public function Process() : void;
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *    The route to match
	 *
	 * @param callable|array $callback
	 *    The callback to call when the route matches
	 */
	public function Get(string $route, callable|array $callback) : void;
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Post(string $route, callable|array $callback) : void;
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Put(string $route, callable|array $callback) : void;
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	
	public function Patch(string $route, callable|array $callback) : void;
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *      The route to match
	 *
	 * @param callable|array $callback
	 *      The callback to call when the route matches
	 */
	
	public function Delete(string $route, callable|array $callback) : void;
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *      The route to match
	 *
	 * @param callable|array $callback
	 *      The callback to call when the route matches
	 */
	public function Head(string $route, callable|array $callback) : void;
}