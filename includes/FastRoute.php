<?php
declare(strict_types = 1);

namespace Rose\Framework\Router;

use FastRoute\DataGenerator;
use FastRoute\Dispatcher;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser;
use Rose\Framework\Core;
use Rose\Framework\ModuleInterface;
use Rose\Framework\Router\Error\NotAllowed;
use Rose\Framework\Router\Error\NotFound;
use RuntimeException;
use function _;
use function sprintf;

/**
 * The Rose Framework PHPFastRoute router
 *
 * @package Rose\Framework\Router
 */
class FastRoute extends AbstractRouter implements ModuleInterface
{
	/**
	 * The name for the module
	 *
	 * @var string
	 */
	public const string MODULE_NAME = 'FastRoute';
	
	/**
	 * The framework core
	 *
	 * @var Core
	 */
	protected Core $core;
	
	/**
	 * The collector for the routes
	 *
	 * @var RouteCollector
	 */
	protected RouteCollector $routes;
	
	/**
	 * Construct the FastRoute router
	 *
	 * @param Core $core
	 *    The framework core
	 *
	 * @param RouteCollector|null $router
	 *    The route handler class, or created with default options if null
	 */
	public function __construct(Core $core, ?RouteCollector $router = NULL)
	{
		$this->core = $core;
		$this->core->Register($this);
		
		if ($router === NULL)
		{
			$router = new RouteCollector(new RouteParser\Std(), new DataGenerator\GroupCountBased());
		}
		$this->routes = $router;
	}
	
	/**
	 * Get the module name
	 *
	 * @return string
	 */
	public function GetName() : string
	{
		return self::MODULE_NAME;
	}
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string|string[] $method
	 *    HTTP method or array of methods
	 *
	 * @param string $route
	 *    The route itself
	 *
	 * @param callable|array $handler
	 *    The route handler
	 */
	public function AddRoute(array|string $method, string $route, callable|array $handler) : void
	{
		$this->core->Logger()->debug(sprintf('Adding route [%s] %s', $method, $route));
		$this->routes->addRoute($method, $route, $handler);
	}
	
	/**
	 * Create a route group with a common prefix
	 *
	 * All routes created in the given callback will have the given group
	 * prefix prepended
	 *
	 * @param string $prefix
	 *    The route prefix
	 *
	 * @param callable|array $callback
	 *    The route handler
	 */
	public function AddGroup(string $prefix, callable|array $callback) : void
	{
		$this->core->Logger()->debug(sprintf('Adding group %s', $prefix));
		$this->routes->addGroup($prefix, $callback);
	}
	
	/**
	 * Do the processing of the route and call the callback given
	 *
	 * @throws NotFound
	 *    When route is not found
	 *
	 * @throws NotAllowed
	 *    When HTTP method is not allowed
	 *
	 * @throws RuntimeException
	 *    When there are logical problems
	 */
	public function Process() : void
	{
		$this->core->Logger()->debug('Processing the routes');
		
		$dispatcher = new GroupCountBased($this->routes->getData());
		$method = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];
		
		// Strip the request string
		$pos = strpos($uri, '?');
		if ($pos !== FALSE)
		{
			$uri = substr($uri, 0, $pos);
		}
		
		// Dispatch the uri
		$uri = rawurldecode($uri);
		$route = $dispatcher->dispatch($method, $uri);
		
		// Make sure the route is set
		if (!isset($route[0]))
		{
			$this->core->Logger()->error('Invalid return value from the dispatcher');
			throw new RuntimeException(_('Invalid return value from the dispatcher'));
		}
		
		// Verify the dispatcher return code
		switch ($route[0])
		{
		case Dispatcher::FOUND:
			break;
		
		case Dispatcher::NOT_FOUND:
			$this->core->Logger()->info(sprintf('URL not found: %s', $route[0]));
			throw new NotFound();
		
		case Dispatcher::METHOD_NOT_ALLOWED:
			// Allowed methods would be in $route[1]
			$this->core->Logger()->info(sprintf('Method %s is not allowed for: %s', $method, $route[0]));
			throw new NotAllowed();
		
		default:
			$this->core->Logger()->error(sprintf('Unknown dispatcher return code %d', $route[0]));
			throw new RuntimeException(_('Unknown dispatcher return code'));
		}
		
		// Make sure we have route callback
		if (!isset($route[1]))
		{
			$this->core->Logger()->error('Route callback data was not found');
			throw new RuntimeException(_('Route callback data not found'));
		}
		
		// Make sure we have route arguments
		if (!isset($route[2]))
		{
			$this->core->Logger()->error('Route argument data was not found');
			throw new RuntimeException(_('Route argument data not found'));
		}
		
		// Work out the callback
		[, $handler, $args] = $route;
		if (!is_callable($handler))
		{
			if (count($handler) !== 2)
			{
				$this->core->Logger()->error('Handler is not callable, and not array with size of 2');
				throw new RuntimeException(_('Invalid handler'));
			}
			
			[$class, $method] = $handler;
			
			if (!class_exists($class))
			{
				$this->core->Logger()->error(sprintf('Not a valid class: %s', $class));
				throw new RuntimeException(_('Invalid handler'));
			}
			$obj = new $class;
			
			if (!$method($obj, $method))
			{
				$this->core->Logger()->error(sprintf('Not a valid class method for %s: %s', $class, $method));
				throw new RuntimeException(_('Invalid handler'));
			}
			$handler = [$obj, $method];
		}
		
		// Do the callback
		call_user_func_array($handler, $args);
	}
}