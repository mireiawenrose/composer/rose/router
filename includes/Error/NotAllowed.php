<?php
declare(strict_types = 1);

namespace Rose\Framework\Router\Error;

use Throwable;
use function _;

/**
 * HTTP 405 - Method Not Allowed exception
 *
 * @package Rose\Framework\Router
 */
class NotAllowed extends HTTPError
{
	public function __construct(string $message = '', int $code = 0, ?Throwable $previous = NULL)
	{
		if (empty($message))
		{
			$message = _('Method not allowed');
		}
		
		if (!$code)
		{
			$code = 405;
		}
		
		parent::__construct($message, $code, $previous);
	}
}