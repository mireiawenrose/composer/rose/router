<?php
declare(strict_types = 1);

namespace Rose\Framework\Router\Error;

use RuntimeException;

/**
 * Base exception for HTTP errors
 *
 * @package Rose\Framework\Router
 */
abstract class HTTPError extends RuntimeException
{
}