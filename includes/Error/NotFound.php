<?php
declare(strict_types = 1);

namespace Rose\Framework\Router\Error;

use Throwable;
use function _;

/**
 * HTTP 404 - Not Found exception
 *
 * @package Rose\Framework\Router
 */
class NotFound extends HTTPError
{
	public function __construct(string $message = '', int $code = 0, ?Throwable $previous = NULL)
	{
		if (empty($message))
		{
			$message = _('Not found');
		}
		
		if (!$code)
		{
			$code = 404;
		}
		
		parent::__construct($message, $code, $previous);
	}
}