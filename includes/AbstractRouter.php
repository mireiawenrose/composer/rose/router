<?php
declare(strict_types = 1);

namespace Rose\Framework\Router;

/**
 * Helpers for the Router classes
 *
 * @package Rose\Framework\Router
 */
abstract class AbstractRouter implements RouterInterface
{
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Get(string $route, callable|array $callback) : void
	{
		$this->AddRoute(self::METHOD_GET, $route, $callback);
	}
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Post(string $route, callable|array $callback) : void
	{
		$this->AddRoute(self::METHOD_POST, $route, $callback);
	}
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Put(string $route, callable|array $callback) : void
	{
		$this->AddRoute(self::METHOD_PUT, $route, $callback);
	}
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Patch(string $route, callable|array $callback) : void
	{
		$this->AddRoute(self::METHOD_PATCH, $route, $callback);
	}
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Delete(string $route, callable|array $callback) : void
	{
		$this->AddRoute(self::METHOD_DELETE, $route, $callback);
	}
	
	/**
	 * Add a route to the collection
	 *
	 * The syntax used in the $route string depends on the used route parser
	 *
	 * @param string $route
	 *     The route to match
	 *
	 * @param callable|array $callback
	 *     The callback to call when the route matches
	 */
	public function Head(string $route, callable|array $callback) : void
	{
		$this->AddRoute(self::METHOD_HEAD, $route, $callback);
	}
}