# Rose Framework Router
Routing support plugin.

## Requirements
- PHP 8.3
- ext-gettext

## Features
Adds the routing support to the Rose Framework, with PHPFastRoute.
